var toaster_error = function(jqXHR){
	if (jqXHR.status === 0) {
		ons.notification.toast('Terjadi kesalahan. Coba cek koneksi anda.', { buttonLabel: 'Dismiss', timeout: 1500 });
		window.location.href = "ip.html"
	} else if (jqXHR.status == 404) {
		ons.notification.toast('URL server tidak ditemukan.', { buttonLabel: 'Dismiss', timeout: 1500 }).then(function(){
			window.location.href = "ip.html"
		});
		console.error('Requested url not found. [404]');
	} else if (jqXHR.status == 401){
		ons.notification.toast('Cek Auth', { buttonLabel: 'Dismiss', timeout: 1500 }).then(function(){
			window.location.href = "login.html"
		})
	} else if (jqXHR.status == 500) {
		ons.notification.toast('Terjadi Kesalahan Server', { buttonLabel: 'Dismiss', timeout: 1500 });
	} else if (exception === 'parsererror') {
		console.error('Requested JSON parse failed.');
	} else if (exception === 'timeout') {
		console.error('Time out error.');
		// alert('Terjadi kesalahan. Coba cek koneksi anda.')
		ons.notification.toast('Gateway Timeout', { buttonLabel: 'Dismiss', timeout: 1500 });
	} else if (exception === 'abort') {
		console.error('Ajax request aborted.');
	} else {
		console.error('Uncaught Error: ' + jqXHR.responseText);
	}
}